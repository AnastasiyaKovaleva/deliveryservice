/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.delivery.core;

import java.io.Serializable;
import java.time.LocalDate;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "orders")
public class Order implements Serializable {

    // Общие параметры заказа, не относящиеся к какому-либо человеку
    private int orderWeight;                // Вес заказа
    private String orderInside;             // То, что внутри заказа
    private LocalDate orderDate;            // Дата заказа
/**
        // Владелец  паспорта
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "Sender_id")
     Sender sender;

     // Владелец  паспорта
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "Recipient_id")
     Recipient recipient;
 
**/
    @Id
    @SequenceGenerator(name = "orderSeq", sequenceName = "order_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(generator = "orderSeq")
    private Long id;                        // ID заказа, т.е его номер

    @Override
    public String toString() {
        return "ID Заказа: " + id + "\n"
                + "Вес заказа: " + orderWeight + "\n"
                + "Внутри заказа: " + orderInside + "\n";

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getOrderWeight() {
        return orderWeight;
    }

    public void setOrderWeight(int orderWeight) {
        this.orderWeight = orderWeight;
    }

    public String getOrderInside() {
        return orderInside;
    }

    public void setOrderInside(String orderInside) {
        this.orderInside = orderInside;
    }

    public LocalDate getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(LocalDate orderDate) {
        this.orderDate = orderDate;
    }

}
