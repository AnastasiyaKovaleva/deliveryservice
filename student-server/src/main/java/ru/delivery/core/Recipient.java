/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ru.delivery.core;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;


public class Recipient {
    
    
    @Id
    @SequenceGenerator(name = "recipientSeq", sequenceName = "recipient_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(generator = "recipientSeq")
    private Long id;                        // ID заказа, т.е его номер


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
     
    private String recipientAdress;         // Адресс
    private String recipientName;           // Имя
    private String recipientSurename;       // Фамилия
    private String recipientPatronymic;     // Отчество

    public String getRecipientAdress() {
        return recipientAdress;
    }

    public void setRecipientAdress(String recipientAdress) {
        this.recipientAdress = recipientAdress;
    }

    public String getRecipientName() {
        return recipientName;
    }

    public void setRecipientName(String recipientName) {
        this.recipientName = recipientName;
    }

    public String getRecipientSurename() {
        return recipientSurename;
    }

    public void setRecipientSurename(String recipientSurename) {
        this.recipientSurename = recipientSurename;
    }

    public String getRecipientPatronymic() {
        return recipientPatronymic;
    }

    public void setRecipientPatronymic(String recipientPatronymic) {
        this.recipientPatronymic = recipientPatronymic;
    }

    
}
