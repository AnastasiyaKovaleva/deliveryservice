/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ru.delivery.core;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;


public class Sender {

    @Id
    @SequenceGenerator(name = "senderSeq", sequenceName = "sender_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(generator = "senderSeq")
    private Long id;                       

    private String senderAdress;            // Адресс
    private String senderName;              // Имя
    private String senderSurename;          // Фамилия
    private String senderPatronymic;        // Отчество

    public String getSenderAdress() {
        return senderAdress;
    }

    public void setSenderAdress(String senderAdress) {
        this.senderAdress = senderAdress;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public String getSenderSurename() {
        return senderSurename;
    }

    public void setSenderSurename(String senderSurename) {
        this.senderSurename = senderSurename;
    }

    public String getSenderPatronymic() {
        return senderPatronymic;
    }

    public void setSenderPatronymic(String senderPatronymic) {
        this.senderPatronymic = senderPatronymic;
    }

}
