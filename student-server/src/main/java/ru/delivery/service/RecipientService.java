/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ru.delivery.service;

import java.util.List;
import java.util.Optional;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import ru.delivery.core.Recipient;

@ApplicationScoped
public class RecipientService {
     @Inject
    EntityManager em;

    /**
     * Получение полного списка заказов
     * @return
     */
    @Transactional
    public List<Recipient> getAll(){
        List <Recipient> recipients = (List <Recipient>) em.createQuery("Select t from " + Recipient.class.getSimpleName() + " t").getResultList();
        return recipients;
    }

    /**
     * Сохранение заказа в базе данных
     *
     * @param order
     * @return
     */
    @Transactional
    public Recipient save(Recipient recipient){
        em.persist(recipient);
        return recipient;
    }

    @Transactional
    public long delete(long id){
        em.remove(id);
        return id;
    }

    @Transactional
    public Optional<Recipient> find(long id){
        Optional<Recipient> opt = Optional.empty();
        Recipient foundRecipient = em.find(Recipient.class, id);

        if (foundRecipient != null) {
            opt = Optional.of(foundRecipient);
        }

        return opt;
    }
}
