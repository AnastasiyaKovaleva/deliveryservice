package ru.delivery.service;

import java.util.List;
import java.util.Optional;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import ru.delivery.core.Sender;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

@ApplicationScoped
public class SenderService {
      @Inject
    EntityManager em;

    /**
     * Получение полного списка заказов
     * @return
     */
    @Transactional
    public List<Sender> getAll(){
        List <Sender> senders = (List <Sender>) em.createQuery("Select t from " + Sender.class.getSimpleName() + " t").getResultList();
        return senders;
    }

    /**
     * Сохранение заказа в базе данных
     *
     * @param order
     * @return
     */
    @Transactional
    public Sender save(Sender sender){
        em.persist(sender);
        return sender;
    }

    @Transactional
    public long delete(long id){
        em.remove(id);
        return id;
    }

    @Transactional
    public Optional<Sender> find(long id){
        Optional<Sender> opt = Optional.empty();
        Sender foundSender = em.find(Sender.class, id);

        if (foundSender != null) {
            opt = Optional.of(foundSender);
        }

        return opt;
    }
}
