/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ru.delivery.controller;

import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import ru.delivery.core.Sender;
import ru.delivery.service.SenderService;


@Path("/senders")
public class SenderController {
     private static final Logger log = Logger.getLogger(OrderController.class.getName());

    @Inject
    SenderService senderService;

    /**
     * Получение полного списка студентов
     *
     * @return
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)   // Преобразует список в JSON массив
    public List<Sender> getAllOrders() {
        log.log(Level.INFO, "Поступил запрос на получение полного списка заказов");
        List<Sender> senders = senderService.getAll();
        if (senders != null) {
            log.log(Level.INFO, "Получен список отправителей [" + senders.size() + "] записей");
        }

        return senders;
    }
    
    @Path("/{id}")
    @GET                                    //Этот глагол показывает, что будет выполнена команда Get HTTP-протокола
    @Produces(MediaType.APPLICATION_JSON)   // Преобразует список в JSON массив
    public Optional<Sender> getSender(@PathParam("id") long id) {
        log.info("Поступил запрос на получение заказа с id = " + id);
        Optional<Sender> sender = senderService.find(id);
        return sender;
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Sender create(Sender sender) {
        log.log(Level.INFO, "Поступил запрос на сохранение заказа в базе данных");
        Sender savedSender = senderService.save(sender);
        if (savedSender != null) {
            log.log(Level.INFO, "Заказ: [" + savedSender + "] сохранен в базе данных");
        }

        return savedSender;
    }

    /**
     * Метод уадаления из базы данных 
     *
     * @param
     * @return
     */
    @Path("/{id}")
    public long delete(@PathParam("id") long id) {
        log.log(Level.INFO, "Поступил запрос на удаление отправителя из базы данных, идентификатор заказа: [" + id + "]");
        long isDeletedSender = senderService.delete(id);
        log.log(Level.INFO, "Отправитель c номером : [" + isDeletedSender + "] удален из базы данных");
        return isDeletedSender;
    }
}
